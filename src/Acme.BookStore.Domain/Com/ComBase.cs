﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Auditing;
using Volo.Abp.Data;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Entities.Auditing;

namespace Acme.BookStore.Com
{
    public class ComBase<T>:FullAuditedAggregateRoot<Guid>,IHasConcurrencyStamp,IFullAuditedObject,IAuditedObject,ICreationAuditedObject, IHasCreationTime, IMayHaveCreator, IModificationAuditedObject, IHasModificationTime, IDeletionAuditedObject, IHasDeletionTime, ISoftDelete, IComSort
    {
        /// <summary>
        /// 
        /// </summary>
        public ComBase() : base() {

            IsDeleted = false;
        }
        public ComBase(Guid b) : base(b) {
            this.Id = b;
        }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Description = "ID")]
        [Description("ID主键")]
        public override Guid Id { get;protected set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Display(Description = "创建时间")]
        [Description("创建时间")]
        public override DateTime CreationTime { get; set; }


        /// <summary>
        /// 创建人ID
        /// </summary>
        [Display(Description = "创建人ID")]
        [Description("创建人ID")]
        public override Guid? CreatorId { get; set; }


        /// <summary>
        /// 删除ID
        /// </summary>
        [Display(Description = "删除ID")]
        [Description("删除ID")]
        public override Guid? DeleterId { get; set; }

        /// <summary>
        /// 删除时间
        /// </summary>
        [Display(Description = "删除时间")]
        [Description("删除时间")]
        public override DateTime? DeletionTime { get; set; }

        /// <summary>
        /// 是否删除 0 否   1 是
        /// </summary>
        [Display(Description = "是否删除 0 否   1 是")]
        [Description("是否删除 0 否   1 是")]
        public override bool IsDeleted { get; set; }


        /// <summary>
        /// 最后修改时间
        /// </summary>
        [Display(Description = "最后修改时间")]
        [Description("最后修改时间")]
        public override DateTime? LastModificationTime { get; set; }

        /// <summary>
        /// 最后修改人ID
        /// </summary>
        [Display(Description = "最后修改人ID")]
        [Description("最后修改人ID")]
        public override Guid? LastModifierId { get; set; }



        /// <summary>
        /// 备注
        /// </summary>
        [StringLength(1000)]
        [Display(Description = "备注")]
        [Description("备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Display(Description = "排序")]
        [Description("排序")]
        public int Sort { get; set; }
    }
}
