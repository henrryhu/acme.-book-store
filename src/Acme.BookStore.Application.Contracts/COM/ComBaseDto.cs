﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Acme.BookStore.COM
{
    /// <summary>
    /// 字典父类Dto
    /// </summary>
    public class ComBaseDto: InputIntIdDto
    {

        public ComBaseDto()
        {
            IsDeleted = false;
            Id=Guid.NewGuid();
            CreationTime=DateTime.Now;
            LastModificationTime = DateTime.Now;
        }
        public Guid Id { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        
        public DateTime? CreationTime { get; set; }

        /// <summary>
        /// 时间格式化
        /// </summary>
          public string CreationTimeFormat { get { return CreationTime.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss"); } }
     

        /// <summary>
        /// 创建人ID
        /// </summary>
        public Guid? CreatorId { get; set; }

    

        /// <summary>
        /// 删除时间
        /// </summary>
        public DateTime? DeletionTime { get; set; }
        /// <summary>
        /// 时间格式化
        /// </summary>
        public string DeletionTimeFormat { get { return DeletionTime.HasValue ? DeletionTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : ""; } }
        /// <summary>
        /// 是否删除 0 否   1 是
        /// </summary>
        public bool IsDeleted { get; set; }


        /// <summary>
        /// 最后修改时间
        /// </summary>
        public DateTime? LastModificationTime { get; set; }

        /// <summary>
        /// 时间格式化
        /// </summary>
        public string LastModificationTimeFormat { get { return LastModificationTime.HasValue ? LastModificationTime.Value.ToString("yyyy-MM-dd HH:mm:ss") : ""; } }

        /// <summary>
        /// 最后修改人ID
        /// </summary>
        public Guid? LastModifierId { get; set; }



        /// <summary>
        /// 备注
        /// </summary>
        [Column("备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

      
    }
}
