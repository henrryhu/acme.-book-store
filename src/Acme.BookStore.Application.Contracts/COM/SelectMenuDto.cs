﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Acme.BookStore.COM
{
    /// <summary>
    /// 查询
    /// </summary>
    public class SelectMenuDto
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单名称")]
        public string Title { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单代码")]
        public string Key { get; set; }
    }
}
