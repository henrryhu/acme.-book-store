﻿using Acme.BookStore.DoMainService;
using Acme.BookStore.Localization;
using AutoMapper.Internal.Mappers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.Caching;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Localization;


namespace Acme.BookStore
{
    /* Inherit your application services from this class.
    */
    public abstract class AcmeAppService : ApplicationService
    {
        protected AcmeAppService()
        {
            LocalizationResource = typeof(AcmeResource);
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="tableName"></param>
        public void ClearCache(string tableName)
        {
            // Type t = typeof(TEntity);
            IDataCacheDomainService CacheData = LazyServiceProvider.LazyGetService<IDataCacheDomainService>();
            string table = tableName;
            CacheData.ClearCache(table + "Dto");
            CacheData.ClearCache(table + "Core");
            CacheData.ClearCache(table);
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="id"></param>
        public void ClearCache(string tableName, int id)
        {
            // Type t = typeof(TEntity);
            IDataCacheDomainService CacheData = LazyServiceProvider.LazyGetService<IDataCacheDomainService>();
            string table = tableName;
            CacheData.ClearCache(table + "Dto");
            CacheData.ClearCache(table + "Core");
            CacheData.ClearCache(table);
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="code"></param>
        public void ClearCache(string tableName, string code)
        {
            // Type t = typeof(TEntity);
            IDataCacheDomainService CacheData = LazyServiceProvider.LazyGetService<IDataCacheDomainService>();
            string table = tableName;
            CacheData.ClearCache(table + "Dto");
            CacheData.ClearCache(table + "Core");
            CacheData.ClearCache(table);
        }


        /// <summary>
        /// 验证身份证是否合法  15 和  18位两种
        /// </summary>
        /// <param name="idCard">要验证的身份证</param>
        protected static bool IsIdCard(string idCard)
        {
            if (string.IsNullOrEmpty(idCard))
            {
                return false;
            }

            if (idCard.Length == 15)
            {
                return Regex.IsMatch(idCard, @"^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$");
            }
            else if (idCard.Length == 18)
            {
                return Regex.IsMatch(idCard, @"^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[A-Z])$", RegexOptions.IgnoreCase);
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 检测是否有中文字符
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static bool IsHasCHZN(string inputData)
        {
            string pattern = "[\u4e00-\u9fbb]";
            return Regex.IsMatch(inputData, pattern);
        }

        /// <summary>
        /// 手机有效性
        /// </summary>
        /// <param name="mobile"></param>
        /// <returns></returns>
        public static bool IsValidMobile(string mobile)
        {
            Regex rx = new Regex(@"^(13|15|17|18|19)\d{9}$", RegexOptions.None);
            Match m = rx.Match(mobile);
            return m.Success;
        }

        /// <summary>
        /// 获取客户端真实IP
        /// </summary>
        /// <returns></returns>
        //protected virtual string GetClientIpAddress()
        //{
        //    try
        //    {
        //        IHttpContextAccessor _httpContextAccessor = LazyServiceProvider.LazyGetService<IHttpContextAccessor>();

        //        var httpContext = _httpContextAccessor.HttpContext;
        //        var headers = httpContext?.Request?.Headers;
        //        if (headers != null && headers.ContainsKey("X-Forwarded-For"))
        //        {
        //            httpContext.Connection.RemoteIpAddress = IPAddress.Parse(headers["X-Forwarded-For"].FirstOrDefault().ToString());
        //        }
        //        return httpContext?.Connection?.RemoteIpAddress?.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LogException(ex, LogLevel.Warning);
        //        return null;
        //    }

        //}
    }
}