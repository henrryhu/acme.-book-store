﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.BookStore
{
    public static class GLisConsts
    {
        public const string DbTablePrefix = "Abp";
        public const string ID4DbTablePrefix = "IdentityServer";

        public const string DbSchema = null;

        /// <summary>
        /// 字典缓存时间
        /// </summary>
        public const int CacheHour = 8;
    }
}
