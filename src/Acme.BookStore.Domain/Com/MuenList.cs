﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Acme.BookStore.Com
{
    /// <summary>
    /// 菜单
    /// </summary>
    [Table("MuenList")]
    [Description("菜单表")]
    public class MuenList : AuditedAggregateRoot<Guid>
    {
        [Key]
        public Guid? Id { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [StringLength(50)]
        [Description("菜单名称")]
        public string Title { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Description("菜单代码")]
        public string Key { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Description("菜单图标")]
        public string IconType { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Description("菜单路径")]
        public string Path { get; set; }
        ///// <summary>
        ///// 菜单名称
        ///// </summary>
        //public List<MuenList> Children { get; set; }
        /// <summary>
        /// 租户ID
        /// </summary>
        [Description("租户ID")]
        public Guid? TenantId { get; set; }
    }
}
