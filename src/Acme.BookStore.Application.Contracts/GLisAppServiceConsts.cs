﻿namespace Acme.BookStore
{
    /// <summary>
    /// 应用服务常量
    /// </summary>
    public class GLisAppServiceConsts
    {
        /// <summary>
        /// 
        /// </summary>
        public const string DataCenterReportQue = "DataCenterReportQue";

        /// <summary>
        /// baseOupputDto   返回错误状态码
        /// </summary>
        public const int SuccessCode = 0;

        /// <summary>
        /// baseOupputDto   返回错误状态码
        /// </summary>
        public const int ErrorCode = 1;

        /// <summary>
        ///     返回错误状态码
        /// </summary>
        public const int WarnCode = 3;
        /// <summary>
        /// 令牌失效
        /// </summary>
        public const int InvalidToken = -1;

        /// <summary>
        /// 用户账户默认密码
        /// </summary>
        public const string DefaultPassword = "GLis123!";
        /// <summary>
        /// 用户账户默认角色名
        /// </summary>
        public const string DefaultRolename = "defaultrole";
    }
}