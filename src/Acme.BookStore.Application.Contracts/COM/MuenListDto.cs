﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.COM
{
    /// <summary>
    /// 菜单
    /// </summary>
    public class MuenListDto: AuditedEntityDto<Guid>
    {
        //public MuenListDto() {
        //    //Children = new List<MuenListDto>();

        //}
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单名称")]
        public string Title { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单代码")]
        public string Key { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单图标")]
        public string IconType { get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [Column("菜单路径")]
        public string Path { get; set; }
        ///// <summary>
        ///// 菜单名称
        ///// </summary>
        //public List<MuenListDto>? Children { get; set; } 
        /// <summary>
        /// 租户ID
        /// </summary>
        public Guid? TenantId { get; set; }
    }
}
