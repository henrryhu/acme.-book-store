﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.BookStore
{
    /// <summary>
    /// 所有方法执行返回dto不要返回原始数据类型
    /// </summary>
    public class OutputBaseDto<T> : OutputBase
    {

        /// <summary>
        /// 如果需要返回其他数据
        /// </summary>
        public T Data { get; set; }
    }
}
