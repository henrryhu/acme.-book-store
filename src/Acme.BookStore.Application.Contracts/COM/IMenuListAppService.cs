﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace Acme.BookStore.COM
{
    public interface IMenuListAppService : ICrudAppService< MuenListDto, Guid, MuenListDto, MuenListDto>
    {
        /// <summary>
        /// 新增接口
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //Task<OutputBaseDto<MuenListDto>> CreateAsync(MuenListDto input);

        ///// <summary>
        ///// 查询
        ///// </summary>
        ///// <returns></returns>
        //Task<OutputBaseDto<MuenListDto>> GetMenuList(SelectMenuDto input);
    }
}
