﻿namespace Acme.BookStore
{
    public class OutputBase
    {

        public OutputBase()
        {
            Msg = "操作成功";
            Code = GLisAppServiceConsts.SuccessCode;
        }
        /// <summary>
        /// 0 成功   1 失败  3 警告  99其他
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// 返回消息内容
        /// </summary>
        public string Msg { get; set; }
    }
}