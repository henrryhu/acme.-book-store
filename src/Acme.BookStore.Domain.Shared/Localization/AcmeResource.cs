﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Localization;

namespace Acme.BookStore.Localization
{

    [LocalizationResourceName("Acme")]
    public class AcmeResource
    {

    }
}
