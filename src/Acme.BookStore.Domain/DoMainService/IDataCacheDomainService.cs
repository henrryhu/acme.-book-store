﻿using Acme.BookStore.Com;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Domain.Services;

namespace Acme.BookStore.DoMainService
{
    public interface IDataCacheDomainService : IDomainService
    {
        #region 系统
        /// <summary>
        /// 医院
        /// </summary>
        /// <returns></returns>
        List<MuenList> GetMuenList();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        void ClearCache(string table);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="key"></param>
        Task AddKeysAsync(string table, string key);
        #endregion
    }
}
