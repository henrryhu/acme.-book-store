﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acme.BookStore.Com
{
    public interface IComSort
    {
        int Sort { get; set; }
    }
}
